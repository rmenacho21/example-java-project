package jalauni.softdev.ui.atoms;

import javafx.scene.control.Label;
import javafx.scene.text.Font;

/**
 * Custom Label.
 * This is a class where stylizes all the labels.
 */
public class AppLabel extends Label {

  /**
   * This is the constructor of Custom Label
   * and contains.
   *
   * @param text This is the text into the label.
   * @param size This is the size of the text.
   */
  public AppLabel(String text, int size) {
    setText(text);
    setFont(Font.font("Henny Penny", size));
    setStyle("-fx-padding: -10px 65px -10px 65px;");
  }
}

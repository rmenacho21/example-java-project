package jalauni.softdev.ui.atoms;

import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.text.Font;

/**
 * Custom Button.
 * This is a class where stylizes all the Buttons.
 */
public class AppButton extends Button {

  /**
   * This is the constructor of Custom Button
   * and contains.
   *
   * @param text This is the text into the Button.
   * @param size This is the size of the text.
   */
  public AppButton(String text, int size) {
    setText(text);
    setFont(Font.font("Modeka", size));
    setCursor(Cursor.HAND);
    setStyle("-fx-background-color: #cc8900;");
  }
}

package jalauni.softdev.ui.atoms;

import javafx.scene.Cursor;
import javafx.scene.control.Button;

/**
 * Custom Image Button.
 * This class help us to create image with a function
 * of a traditional button and in this class we
 * style this Image Button.
 */
public class ButtonWithImage extends Button {

  /**
   * This is the Constructor of CustomImageButton
   * and Contains two params.
   *
   * @param image the image you need.
   */
  public ButtonWithImage(String image) {
    setCursor(Cursor.HAND);
    setStyle("-fx-focus-color: transparent;");
    setStyle("-fx-background-color: transparent;");
    setGraphic(new AppImageView(image));
  }
}

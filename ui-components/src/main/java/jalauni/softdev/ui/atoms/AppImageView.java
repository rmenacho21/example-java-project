package jalauni.softdev.ui.atoms;

import javafx.scene.image.ImageView;

/**
 * Custom Image.
 * This class helps us to import
 * the images of the resources' directory
 * whit two params the team and the image
 * like /images/image.png.
 */
public class AppImageView extends ImageView {

  public AppImageView(String image) {
    super(image);
  }

  /**
   * CustomImage Method.
   * This method create an Image with a size X and size Y.
   */
  public AppImageView(String image, int sizeX, int sizeY) {
    super(image);
    setFitWidth(sizeX);
    setFitHeight(sizeY);
  }

}

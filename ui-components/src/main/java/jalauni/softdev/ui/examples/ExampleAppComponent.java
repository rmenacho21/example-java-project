package jalauni.softdev.ui.examples;

import jalauni.softdev.ui.common.WindowComponent;
import jalauni.softdev.ui.molecules.ExampleInputTextWithLabel;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * Example class.
 */
public class ExampleAppComponent implements WindowComponent {
  @Override
  public String getWindowTitle() {
    return "Example Window";
  }

  @Override
  public Node getWindowComponent() {
    String javaVersion = System.getProperty("java.version");
    String javafxVersion = System.getProperty("javafx.version");

    var javaVersionText =
        new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");

    var verticalBox = new VBox();
    var exampleLabel = new ExampleInputTextWithLabel();

    verticalBox.getChildren().addAll(javaVersionText, exampleLabel.build());
    return verticalBox;
  }
}

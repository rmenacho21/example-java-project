package jalauni.softdev.ui.common;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Component that makes the abstraction of a component that opens a new window.
 */
public interface WindowComponent {

  String getWindowTitle();

  default int getWidth() {
    return 600;
  }

  default int getHeight() {
    return 600;
  }

  Node getWindowComponent();

  default Stage getWindow() {
    return new Stage();
  }

  /**
   * This method generates a new window rendering the component.
   */
  default void openWindow() {
    Stage window = getWindow();
    openWindow(window);
  }

  /**
   * This generates a new window rendering the component.
   *
   * @param window previously created Stage object
   */
  default void openWindow(Stage window) {
    window.setTitle(getWindowTitle());
    Pane box = new Pane();
    box.getChildren().add(getWindowComponent());
    Scene scene = new Scene(box, getWidth(), getHeight());
    window.setScene(scene);
    window.show();
  }
}

package jalauni.softdev.ui.common;

import javafx.scene.Node;

/**
 * Abstraction of App Component.
 */
public interface Component {

  default Node build() {
    configureComponent();
    return getComponent();
  }

  void configureComponent();

  Node getComponent();
}

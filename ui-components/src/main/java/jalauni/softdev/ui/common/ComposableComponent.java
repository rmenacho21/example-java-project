package jalauni.softdev.ui.common;

import javafx.scene.Node;

/**
 * Abstraction of Component that could be composable.
 */
public interface ComposableComponent extends Component {

  @Override
  default Node build() {
    compose();
    return Component.super.build();
  }

  void compose();
}

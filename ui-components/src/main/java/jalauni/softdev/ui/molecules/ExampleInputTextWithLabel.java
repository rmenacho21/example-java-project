package jalauni.softdev.ui.molecules;

import jalauni.softdev.ui.atoms.AppLabel;
import jalauni.softdev.ui.common.ComposableComponent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 * Example of how to use Composable Component.
 */
public class ExampleInputTextWithLabel implements ComposableComponent {

  private final HBox horizontalBox;

  public ExampleInputTextWithLabel() {
    horizontalBox = new HBox();
  }

  @Override
  public void configureComponent() {
    horizontalBox.setAlignment(Pos.CENTER);
    horizontalBox.setSpacing(10);
  }

  @Override
  public Node getComponent() {
    return horizontalBox;
  }

  @Override
  public void compose() {
    var label = new AppLabel("Label", 10);
    TextField nameInput = new TextField();
    horizontalBox.getChildren().addAll(label, nameInput);
  }
}

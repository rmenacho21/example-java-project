module uicomponents {
  requires javafx.controls;

  exports jalauni.softdev.ui.common;
  exports jalauni.softdev.ui.atoms;
  exports jalauni.softdev.ui.examples;
}
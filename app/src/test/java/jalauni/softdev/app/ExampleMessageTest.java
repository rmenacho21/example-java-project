package jalauni.softdev.app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ExampleMessageTest {

  @Test
  public void exampleTest() {
    var hellomsg = ExampleMessage.sayHello();
    assertEquals("Hello!", hellomsg);
  }

}
package jalauni.softdev.app;

/**
 * Example Message class.
 */
public class ExampleMessage {
  private ExampleMessage() {
    // Prevent instantiation
  }

  public static String sayHello() {
    return "Hello!";
  }
}
